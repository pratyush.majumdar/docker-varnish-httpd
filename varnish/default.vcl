vcl 4.0;

import std;

backend default {
    .host = "httpd";
    .port = "80";
    .first_byte_timeout = 30s;
    .probe = {
        .url = "/";
        .timeout = 2s;
        .interval = 5s;
        .window = 5;
        .threshold = 3;
    }
}

sub vcl_recv {
    
}

# Set the grace period to serve stale content
sub vcl_backend_response {
    set beresp.ttl = 1m;
    set beresp.grace = 2h;

    # Do not cache error responses
    if (beresp.status >= 400) {
        set beresp.uncacheable = true;
    }
}

# Error handling: serve stale content if backend is unavailable
sub vcl_backend_error {
    if (bereq.retries > 0) {
        # If the backend failed, try to deliver a stale object
        return (deliver);
    }

    # Serve a custom 503 error page
    set beresp.status = 503;
    synthetic("<html><body><h1>Service Unavailable</h1><p>The server is currently unavailable. Please try again later.</p></body></html>");
    set beresp.http.Content-Type = "text/html; charset=utf-8";
    return (deliver); # Retry the request to the backend
}

sub vcl_deliver {
    if (obj.hits > 0) {
        set resp.http.X-Cache = "HIT";
        set resp.http.X-Cache-Hits = obj.hits;
    } else {
        set resp.http.X-Cache = "MISS";
    }
}
