# docker-varnish-httpd
A project to start a Varnish frontend and a Apache backend using Docker Compose to test serving of stale content in case the Varnish health check fails.

### Step 1: Build Apache server using Dockerfile
```
docker build -t centos7-php:7.0 .
```

### Step 2: Build Varnish server using Dockerfile
```
docker build -t centos-varnish:5.2.1 .
```

### Step 3: Use docker-compose file to start both the servers within a same network
```
docker-compose up

docker-compose down
```

# Optional commands for testing
```
# start the httpd server
docker run -d --name httpd -p 8080:80 centos7-php:7.0

# test URL
http://localhost:8080

# start the varnish server
docker run -d --name varnish -p 9000:80 -v "$PWD"/default.vcl:/etc/varnish/default.vcl centos-varnish:5.2.1

# test URL
http://localhost:9000
```
